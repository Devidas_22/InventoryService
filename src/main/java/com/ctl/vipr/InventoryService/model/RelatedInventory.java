package com.ctl.vipr.InventoryService.model;

public class RelatedInventory {

	private static final long serialVersionUID = 1L;
	private String accountId;
	private String accountAliasName; //Deivamani - CR6050 - US56399
    private String accountSystemCd;
	private  Long inventoryId;
	private String inventoryTypeCode;
	private String productTypeCode;
	private String serviceAliasName;
	private String serviceId;
	private String serviceTypeCode;
	private Long seatCount; //Amrutha - CR4513
	//PNAIDU: UBI
	private String ubi; 
		
	public String getUbi() {
		return ubi;
	}

	public void setUbi(String ubi) {
		this.ubi = ubi;
	}
	
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getAccountSystemCd() {
		return accountSystemCd;
	}
	public void setAccountSystemCd(String accountSystemCd) {
		this.accountSystemCd = accountSystemCd;
	}
	public Long getInventoryId() {
		return inventoryId;
	}
	public void setInventoryId(Long inventoryId) {
		this.inventoryId = inventoryId;
	}
	public String getInventoryTypeCode() {
		return inventoryTypeCode;
	}
	public void setInventoryTypeCode(String inventoryTypeCode) {
		this.inventoryTypeCode = inventoryTypeCode;
	}
	public String getProductTypeCode() {
		return productTypeCode;
	}
	public void setProductTypeCode(String productTypeCode) {
		this.productTypeCode = productTypeCode;
	}
	public String getServiceAliasName() {
		return serviceAliasName;
	}
	public void setServiceAliasName(String serviceAliasName) {
		this.serviceAliasName = serviceAliasName;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getServiceTypeCode() {
		return serviceTypeCode;
	}
	public void setServiceTypeCode(String serviceTypeCode) {
		this.serviceTypeCode = serviceTypeCode;
	}
	
	public Long getSeatCount() {
		return seatCount;
	}
	public void setSeatCount(Long seatCount) {
		this.seatCount = seatCount;
	}

	public String getAccountAliasName() {
		return accountAliasName;
	}
	public void setAccountAliasName(String accountAliasName) {
		this.accountAliasName = accountAliasName;
	}

	
	
}
