package com.ctl.vipr.InventoryService.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class InventoryLocation {

	private static final long serialVersionUID = -5640301896296145693L;
	
	private String serviceAddressId;
	private String addressLine1;
	private String addressLine2;
	private String addressLine3;
	private String city;
	private String postalCode;
	private String stateProvinceCode;
	private String countryCode;
	
	private Long locationId;
	private String enterpriseId;
	private String locationName;
	private String locationTypeCode;
	private String clliCode;
	private RelatedInventory[] inventory;
	private Long seatCount; //Amrutha - CR4513
	 
	public  String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getStateProvinceCode() {
		return stateProvinceCode;
	}
	public void setStateProvinceCode(String stateProvinceCode) {
		this.stateProvinceCode = stateProvinceCode;
	}
	public Long getLocationId() {
		return locationId;
	}
	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getAddressLine3() {
		return addressLine3;
	}
	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}
	public String getServiceAddressId() {
		return serviceAddressId;
	}
	public void setServiceAddressId(String serviceAddressId) {
		this.serviceAddressId = serviceAddressId;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getLocationTypeCode() {
		return locationTypeCode;
	}
	public void setLocationTypeCode(String locationTypeCode) {
		this.locationTypeCode = locationTypeCode;
	}
	public String getClliCode() {
		return clliCode;
	}
	public void setClliCode(String clliCode) {
		this.clliCode = clliCode;
	}
	public RelatedInventory[] getInventory() {
		return inventory;
	}
	public void setInventory(RelatedInventory[] inventory) {
		this.inventory = inventory;
	}
	public String getEnterpriseId() {
		return enterpriseId;
	}
	public void setEnterpriseId(String enterpriseId) {
		this.enterpriseId = enterpriseId;
	}
	public Long getSeatCount() {
		return seatCount;
	}
	public void setSeatCount(Long seatCount) {
		this.seatCount = seatCount;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InventoryLocation [serviceAddressId=");
		builder.append(serviceAddressId);
		builder.append(", addressLine1=");
		builder.append(addressLine1);
		builder.append(", addressLine2=");
		builder.append(addressLine2);
		builder.append(", addressLine3=");
		builder.append(addressLine3);
		builder.append(", city=");
		builder.append(city);
		builder.append(", postalCode=");
		builder.append(postalCode);
		builder.append(", stateProvinceCode=");
		builder.append(stateProvinceCode);
		builder.append(", countryCode=");
		builder.append(countryCode);
		builder.append(", locationId=");
		builder.append(locationId);
		builder.append(", locationName=");
		builder.append(locationName);
		builder.append(", locationTypeCode=");
		builder.append(locationTypeCode);
		builder.append(", clliCode=");
		builder.append(clliCode);
		builder.append(", inventory=");
		builder.append(Arrays.toString(inventory));
		builder.append(", seatCount=");
		builder.append(seatCount);
		builder.append("]");
		return builder.toString();
	}
	
	public static enum Order implements Comparator<InventoryLocation> {
		addressLine1() {
			@Override
			public int compare(InventoryLocation lhs, InventoryLocation rhs){
				return lhs.addressLine1.compareToIgnoreCase(rhs.addressLine1);
			}
		},city(){
			@Override
			public int compare(InventoryLocation lhs, InventoryLocation rhs){
				return lhs.city.compareToIgnoreCase(rhs.city);
			}
		},postalCode(){
			@Override
			public int compare(InventoryLocation lhs, InventoryLocation rhs){
				return lhs.postalCode.compareToIgnoreCase(rhs.postalCode);
			}
		},stateProvinceCode(){
			@Override
			public int compare(InventoryLocation lhs, InventoryLocation rhs){
				return lhs.stateProvinceCode.compareToIgnoreCase(rhs.stateProvinceCode);
			}
		},countryCode(){
			@Override
			public int compare(InventoryLocation lhs, InventoryLocation rhs){
				return lhs.countryCode.compareToIgnoreCase(rhs.countryCode);
			}
		};
		
		public abstract int compare(InventoryLocation lhs, InventoryLocation rhs);
		
		public Comparator<InventoryLocation> ascending() {
			return this;     
		}
		
		public Comparator<InventoryLocation> descending() {
			return Collections.reverseOrder(this);
		}
		
		public static Order fromValue(String v) {
            for (Order c: Order.values()) {
                if (c.toString().equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }
	}
}