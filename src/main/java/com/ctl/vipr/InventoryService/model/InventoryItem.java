package com.ctl.vipr.InventoryService.model;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;


public class InventoryItem extends DAResponse implements Serializable{

	private static final long serialVersionUID = 1564389567896L;
	
	public InventoryItem(DAResponse response){
		super(response);
	}
	
	public InventoryItem() {
		super();
	}

	private String id; 

	
	private String enterpriseId;
	
	private String serviceId;
	
	private String triksId;
	
	private String componentName;
	
	private Integer componentId;
	
	private String customerAccountId;
	
	private String customerAccountAliasName;
	
	private String limsComponentID;
	
	private String circuitFormat;
	
	private String vendorName;	
	
	private String mgmntType;
	
	private String serverType;

	private String legalEnityCode;

	private String circuitType;

	private String productFamilyCode;

	private String accountSystemCode;
	private String productAccountId;
	private String customerAccountName;

	private InventoryLocation[] location;

	private Long inventoryId;

	private String productTypeCode;

	private String serviceAliasName;
	
	private Long serviceElementId;
	
	private Long serviceCategoryId;
	
	private String statusCode;
	
	private String productFamilyName;
	
	private String portType;
	
	private String bandwidth;
	
	private Boolean detailsDataAvailable;
	
	private Long limsStatusCode;
	
	private Long portId;
	
	private String inventoryTypeCode;
	
	private RelatedInventory[] childInventory;
	
	private RelatedInventory[] parentInventory;
	
	private boolean hasMSParent;
	
	private Boolean exchangeFlag;
	
	private Long seatCount; //Amrutha - CR4513
	
	/**
	 * Below fields are used for Notification Inventories
	 */	
	
	private boolean serviceIndicator;
	
	private boolean accountIndicator;

	/**
	 * Field src is added to find the source of inventoryItem , whether it is from QCTRL,LIMS or TMS etc
	 */
	
	private int src;

	/**
	 * Below fields are for DedicatedLD Inventory
	 */	
	
	private String switchId;
	
	private String numericCircuitId;

	/**
	 * Below fields are for VOIP Inventory
	 */
	
	private String serviceTypeCode;
	
	private Long tenantId;

	/**
	 * Below fields are used for IQ and NBS Inventories
	 */	
	
	private CUGDetails[] cugs;	
	
	private String network;

	/**
	 * Below fields are used for TollFreeInventory Summary Inventory
	 */	
	
	private List<TollFreeFeature> tollFreeFeatures;
	
	private String callPlan;	
	
	private String termination_ringTo;
	
	private String termination_trunkGroup;
	
	private String termination_dnisName;
	
	private String group_callPlan_cd;
	
	private String group_callPlan_desc;
	
	private String tollFree_desc;
	
	private String serviceId_name;
	
	private String tollFree_number;

	/**
	 * Below fields are used for PL Inventory
	 */
	
	private Boolean diversityIndicator;
	
	private String installationDate;
	
	private String contractTerminationDate;
	
	private String tirksCircuitId;

	/**
	 * Cloud
	 */	
	
	private String btlServiceId;
	
	private String subProductCode;

	/**
	 * ESP (Enterprise Session Pool) Inventory
	 */	
	
	private String poolType;	
	
	private Long totalSessions;
	
	private Long availableSessions;
	
	private String serviceName;
	
	private String sipSwitchId;

	/**
	 * MOE Inventory
	 */
	
	private Boolean qosIndicator;
	
	private String bansAccountNumber;

	/**
	 * Used for Both QCI and MOE Inventories
	 */
	
	private List<EVCDetails> evcDetailsList;

	/**
	 * LD specific
	 */	
	
	private String componentGroupCode;
	
	private String componentGroupValue;
	
	/**
	 * NM Specific
	 */
	
	private String managedIp;
	
	private String managedIpV6;
	
	private String realIp;
	
	private String realIpV6;

	/* NM combinedInventory */
	
	private String deviceName;
	
	
	private String state;
	
	private String countryDesc;
	
	private String postBoxNo;
	
	private String careOfName;
	
	private String route;
	
	private String streetNumber;
	
	private String streetName;
	
	private String streetType;
	
	private String npa;
	
	private String nxx;
	
	private String countryShortCode;
	
	// added these twi fields for Ordering initiative 2014 May - mxrao2
	
	private String billingName1;
	
	private String billingName2;
	
	private boolean militaryAddress;
	
	//added for DVAR
	
	private String locality;
	
	private String primaryLine;
	
	private String singleLineAddress;
	
	
	private String addrLine1;
	
	private String addrLine2;
	
	private String addrLine3;
	
	private String stateProvinceCode;
	
	private String countryCode;
	
	private String country;
	
	private String city;
	
	private String zip;
	
	//PNAIDU: UBI
	
	private String ubi; 
	
	//added for SDWAN
	
	private String applianceName;
	
	public String getApplianceName() {
		return applianceName;
	}

	public void setApplianceName(String applianceName) {
		this.applianceName = applianceName;
	}

	public String getUbi() {
		return ubi;
	}

	public void setUbi(String ubi) {
		this.ubi = ubi;
	}

	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public boolean isHasMSParent() {
		return hasMSParent;
	}
	
	public void setHasMSParent(boolean hasMSParent) {
		this.hasMSParent = hasMSParent;
	}
	public String getCircuitType() {
		return circuitType;
	}
	public void setCircuitType(String circuitType) {
		this.circuitType = circuitType;
	}
	public int getSrc() {
		return src;
	}
	public void setSrc(int src) {
		this.src = src;
	}
	public String getTriksId() {
		return triksId;
	}
	public void setTriksId(String triksId) {
		this.triksId = triksId;
	}
	public String getComponentName() {
		return componentName;
	}
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	public String getCustomerAccountId() {
		return customerAccountId;
	}
	public String getCustomerAccountAliasName() {
		return customerAccountAliasName;
	}

	public void setCustomerAccountAliasName(String customerAccountAliasName) {
		this.customerAccountAliasName = customerAccountAliasName;
	}

	public String getSipSwitchId() {
		return sipSwitchId;
	}
	public void setSipSwitchId(String sipSwitchId) {
		this.sipSwitchId = sipSwitchId;
	}
	public void setCustomerAccountId(String customerAccountId) {
		this.customerAccountId = customerAccountId;
	}
	public String getCustomerAccountName() {
		return customerAccountName;
	}
	public void setCustomerAccountName(String customerAccountName) {
		this.customerAccountName = customerAccountName;
	}
	public String getLimsComponentID() {
		return limsComponentID;
	}
	public void setLimsComponentID(String limsComponentID) {
		this.limsComponentID = limsComponentID;
	}
	public String getCircuitFormat() {
		return circuitFormat;
	}
	public void setCircuitFormat(String circuitFormat) {
		this.circuitFormat = circuitFormat;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}	
	public String getMgmntType() {
		return mgmntType;
	}
	public void setMgmntType(String mgmntType) {
		this.mgmntType = mgmntType;
	}
	public String getServerType() {
		return serverType;
	}
	public void setServerType(String serverType) {
		this.serverType = serverType;
	}
	public String getLegalEnityCode() {
		return legalEnityCode;
	}
	public void setLegalEnityCode(String legalEnityCode) {
		this.legalEnityCode = legalEnityCode;
	}
	public String getProductFamilyCode() {
		return productFamilyCode;
	}
	public void setProductFamilyCode(String productFamilyCode) {
		this.productFamilyCode = productFamilyCode;
	}
	public String getAccountSystemCode() {
		return accountSystemCode;
	}
	public void setAccountSystemCode(String accountSystemCode) {
		this.accountSystemCode = accountSystemCode;
	}
	public String getProductAccountId() {
		return productAccountId;
	}
	public void setProductAccountId(String productAccountId) {
		this.productAccountId = productAccountId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getEnterpriseId() {
		return enterpriseId;
	}
	public void setEnterpriseId(String enterpriseId) {
		this.enterpriseId = enterpriseId;
	}
	public String getProductTypeCode() {
		return productTypeCode;
	}
	public void setProductTypeCode(String productTypeCode) {
		this.productTypeCode = productTypeCode;
	}
	public String getServiceAliasName() {
		return serviceAliasName;
	}
	public void setServiceAliasName(String serviceAliasName) {
		this.serviceAliasName = serviceAliasName;
	}
	public Long getServiceElementId() {
		return serviceElementId;
	}
	public void setServiceElementId(Long serviceElementId) {
		this.serviceElementId = serviceElementId;
	}
	public Long getServiceCategoryId() {
		return serviceCategoryId;
	}
	public void setServiceCategoryId(Long serviceCategoryId) {
		this.serviceCategoryId = serviceCategoryId;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getProductFamilyName() {
		return productFamilyName;
	}
	public void setProductFamilyName(String productFamilyName) {
		this.productFamilyName = productFamilyName;
	}
	public Long getInventoryId() {
		return inventoryId;
	}
	public void setInventoryId(Long inventoryId) {
		this.inventoryId = inventoryId;
	}
	public String getSwitchId() {
		return switchId;
	}
	public void setSwitchId(String switchId) {
		this.switchId = switchId;
	}
	public String getNumericCircuitId() {
		return numericCircuitId;
	}
	public void setNumericCircuitId(String numericCircuitId) {
		this.numericCircuitId = numericCircuitId;
	}
	public String getServiceTypeCode() {
		return serviceTypeCode;
	}
	public void setServiceTypeCode(String serviceTypeCode) {
		this.serviceTypeCode = serviceTypeCode;
	}
	public Long getTenantId() {
		return tenantId;
	}
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	public String getPortType() {
		return portType;
	}
	public void setPortType(String portType) {
		this.portType = portType;
	}
	public String getBandwidth() {
		return bandwidth;
	}
	public void setBandwidth(String bandwidth) {
		this.bandwidth = bandwidth;
	}
	public Boolean getDetailsDataAvailable() {
		return detailsDataAvailable;
	}
	public void setDetailsDataAvailable(Boolean detailsDataAvailable) {
		this.detailsDataAvailable = detailsDataAvailable;
	}
	public Long getLimsStatusCode() {
		return limsStatusCode;
	}
	public void setLimsStatusCode(Long limsStatusCode) {
		this.limsStatusCode = limsStatusCode;
	}
	public Long getPortId() {
		return portId;
	}
	public void setPortId(Long portId) {
		this.portId = portId;
	}
	public CUGDetails[] getCugs() {
		return cugs;
	}
	public void setCugs(CUGDetails[] cugs) {
		this.cugs = cugs;
	}
	public String getNetwork() {
		return network;
	}
	public void setNetwork(String network) {
		this.network = network;
	}
	public List<TollFreeFeature> getTollFreeFeatures() {
		return tollFreeFeatures;
	}
	public void setTollFreeFeatures(List<TollFreeFeature> tollFreeFeatures) {
		this.tollFreeFeatures = tollFreeFeatures;
	}
	public String getCallPlan() {
		return callPlan;
	}
	public void setCallPlan(String callPlan) {
		this.callPlan = callPlan;
	}
	public String getTermination_ringTo() {
		return termination_ringTo;
	}
	public void setTermination_ringTo(String termination_ringTo) {
		this.termination_ringTo = termination_ringTo;
	}
	public String getTermination_trunkGroup() {
		return termination_trunkGroup;
	}
	public void setTermination_trunkGroup(String termination_trunkGroup) {
		this.termination_trunkGroup = termination_trunkGroup;
	}
	public String getTermination_dnisName() {
		return termination_dnisName;
	}
	public void setTermination_dnisName(String termination_dnisName) {
		this.termination_dnisName = termination_dnisName;
	}
	public String getGroup_callPlan_cd() {
		return group_callPlan_cd;
	}
	public void setGroup_callPlan_cd(String group_callPlan_cd) {
		this.group_callPlan_cd = group_callPlan_cd;
	}
	public String getGroup_callPlan_desc() {
		return group_callPlan_desc;
	}
	public void setGroup_callPlan_desc(String group_callPlan_desc) {
		this.group_callPlan_desc = group_callPlan_desc;
	}
	public String getTollFree_desc() {
		return tollFree_desc;
	}
	public void setTollFree_desc(String tollFree_desc) {
		this.tollFree_desc = tollFree_desc;
	}
	public String getServiceId_name() {
		return serviceId_name;
	}
	public void setServiceId_name(String serviceId_name) {
		this.serviceId_name = serviceId_name;
	}
	public String getTollFree_number() {
		return tollFree_number;
	}
	public void setTollFree_number(String tollFree_number) {
		this.tollFree_number = tollFree_number;
	}
	public Boolean getDiversityIndicator() {
		return diversityIndicator;
	}
	public void setDiversityIndicator(Boolean diversityIndicator) {
		this.diversityIndicator = diversityIndicator;
	}
	public String getInstallationDate() {
		return installationDate;
	}
	public void setInstallationDate(String installationDate) {
		this.installationDate = installationDate;
	}
	public String getContractTerminationDate() {
		return contractTerminationDate;
	}
	public void setContractTerminationDate(String contractTerminationDate) {
		this.contractTerminationDate = contractTerminationDate;
	}
	public String getTirksCircuitId() {
		return tirksCircuitId;
	}
	public void setTirksCircuitId(String tirksCircuitId) {
		this.tirksCircuitId = tirksCircuitId;
	}
	public String getBtlServiceId() {
		return btlServiceId;
	}
	public void setBtlServiceId(String btlServiceId) {
		this.btlServiceId = btlServiceId;
	}
	public String getSubProductCode() {
		return subProductCode;
	}
	public void setSubProductCode(String subProductCode) {
		this.subProductCode = subProductCode;
	}
	public String getPoolType() {
		return poolType;
	}
	public void setPoolType(String poolType) {
		this.poolType = poolType;
	}	
	public Long getTotalSessions() {
		return totalSessions;
	}
	public void setTotalSessions(Long totalSessions) {
		this.totalSessions = totalSessions;
	}
	public Long getAvailableSessions() {
		return availableSessions;
	}
	public void setAvailableSessions(Long availableSessions) {
		this.availableSessions = availableSessions;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}	
	public String getBansAccountNumber() {
		return bansAccountNumber;
	}
	public void setBansAccountNumber(String bansAccountNumber) {
		this.bansAccountNumber = bansAccountNumber;
	}
	public List<EVCDetails> getEvcDetailsList() {
		return evcDetailsList;
	}
	public void setEvcDetailsList(List<EVCDetails> evcDetailsList) {
		this.evcDetailsList = evcDetailsList;
	}
	public String getComponentGroupCode() {
		return componentGroupCode;
	}
	public void setComponentGroupCode(String componentGroupCode) {
		this.componentGroupCode = componentGroupCode;
	}
	public String getComponentGroupValue() {
		return componentGroupValue;
	}
	public void setComponentGroupValue(String componentGroupValue) {
		this.componentGroupValue = componentGroupValue;
	}
	public String getInventoryTypeCode() {
		return inventoryTypeCode;
	}
	public void setInventoryTypeCode(String inventoryTypeCode) {
		this.inventoryTypeCode = inventoryTypeCode;
	}
	public RelatedInventory[] getChildInventory() {
		return childInventory;
	}
	public void setChildInventory(RelatedInventory[] childInventory) {
		this.childInventory = childInventory;
	}
	public RelatedInventory[] getParentInventory() {
		return parentInventory;
	}
	public void setParentInventory(RelatedInventory[] parentInventory) {
		this.parentInventory = parentInventory;
	}
	public String getManagedIp() {
		return managedIp;
	}
	public void setManagedIp(String managedIp) {
		this.managedIp = managedIp;
	}
	public String getManagedIpV6() {
		return managedIpV6;
	}
	public void setManagedIpV6(String managedIpV6) {
		this.managedIpV6 = managedIpV6;
	}
	public String getRealIp() {
		return realIp;
	}
	public void setRealIp(String realIp) {
		this.realIp = realIp;
	}
	public String getRealIpV6() {
		return realIpV6;
	}
	public void setRealIpV6(String realIpV6) {
		this.realIpV6 = realIpV6;
	}
	public InventoryLocation[] getLocation() {
		return location;
	}
	public void setLocation(InventoryLocation[] location) {
		this.location = location;
	}
	public Boolean getQosIndicator() {
		return qosIndicator;
	}
	public void setQosIndicator(Boolean qosIndicator) {
		this.qosIndicator = qosIndicator;
	}
	public Boolean isExchangeFlag() {
		return exchangeFlag;
	}
	public void setExchangeFlag(Boolean exchangeFlag) {
		this.exchangeFlag = exchangeFlag;
	}
	public boolean isServiceIndicator() {
		return serviceIndicator;
	}
	public void setServiceIndicator(boolean serviceIndicator) {
		this.serviceIndicator = serviceIndicator;
	}
	public boolean isAccountIndicator() {
		return accountIndicator;
	}
	public void setAccountIndicator(boolean accountIndicator) {
		this.accountIndicator = accountIndicator;
	}

	public String getNpa() {
		return npa;
	}
	public void setNpa(String npa) {
		this.npa = npa;
	}
	public String getNxx() {
		return nxx;
	}
	public void setNxx(String nxx) {
		this.nxx = nxx;
	}	
	public String getStreetNumber() {
		return streetNumber;
	}
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	public String getStreetType() {
		return streetType;
	}
	public void setStreetType(String streetType) {
		this.streetType = streetType;
	}
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
		
	
	/**
	 * @return the countryDesc
	 */
	public String getCountryDesc() {
		return countryDesc;
	}
	/**
	 * @param countryDesc the countryDesc to set
	 */
	public void setCountryDesc(String countryDesc) {
		this.countryDesc = countryDesc;
	}
	/**
	 * @return the careOfName
	 */
	public String getCareOfName() {
		return careOfName;
	}
	/**
	 * @param careOfName the careOfName to set
	 */
	public void setCareOfName(String careOfName) {
		this.careOfName = careOfName;
	}
	/**
	 * @return the postBoxNo
	 */
	public String getPostBoxNo() {
		return postBoxNo;
	}
	/**
	 * @param postBoxNo the postBoxNo to set
	 */
	public void setPostBoxNo(String postBoxNo) {
		this.postBoxNo = postBoxNo;
	}
	/**
	 * @return the route
	 */
	public String getRoute() {
		return route;
	}
	/**
	 * @param route the route to set
	 */
	public void setRoute(String route) {
		this.route = route;
	}
	public String getCountryShortCode() {
		return countryShortCode;
	}
	public void setCountryShortCode(String countryShortCode) {
		this.countryShortCode = countryShortCode;
	}
	public String getBillingName1() {
		return billingName1;
	}
	public void setBillingName1(String billingName1) {
		this.billingName1 = billingName1;
	}
	public String getBillingName2() {
		return billingName2;
	}
	public void setBillingName2(String billingName2) {
		this.billingName2 = billingName2;
	}
	public boolean isMilitaryAddress() {
		return militaryAddress;
	}
	public void setMilitaryAddress(boolean militaryAddress) {
		this.militaryAddress = militaryAddress;
	}
	
	public String getLocality() {
		return locality;
	}
	public void setLocality(String locality) {
		this.locality = locality;
	}
	public String getPrimaryLine() {
		return primaryLine;
	}
	public void setPrimaryLine(String primaryLine) {
		this.primaryLine = primaryLine;
	}
	public String getSingleLineAddress() {
		return singleLineAddress;
	}
	public void setSingleLineAddress(String singleLineAddress) {
		this.singleLineAddress = singleLineAddress;
	}
	
	public String getAddrLine1() {
		return addrLine1;
	}

	public void setAddrLine1(String addrLine1) {
		this.addrLine1 = addrLine1;
	}

	public String getAddrLine2() {
		return addrLine2;
	}

	public void setAddrLine2(String addrLine2) {
		this.addrLine2 = addrLine2;
	}

	public String getAddrLine3() {
		return addrLine3;
	}

	public void setAddrLine3(String addrLine3) {
		this.addrLine3 = addrLine3;
	}

	public String getStateProvinceCode() {
		return stateProvinceCode;
	}

	public void setStateProvinceCode(String stateProvinceCode) {
		this.stateProvinceCode = stateProvinceCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}
	
	
	public static enum Order implements Comparator<InventoryItem> {
		serviceId() {
			@Override
			public int compare(InventoryItem lhs, InventoryItem rhs){
				return lhs.serviceId.compareToIgnoreCase(rhs.serviceId);
			}
		},productTypeCode(){
			@Override
			public int compare(InventoryItem lhs, InventoryItem rhs){
				return lhs.productTypeCode.compareToIgnoreCase(rhs.productTypeCode);
			}
		},serviceAliasName(){
			@Override
			public int compare(InventoryItem lhs, InventoryItem rhs){
				return lhs.serviceAliasName.compareToIgnoreCase(rhs.serviceAliasName);
			}
		},customerAccountId(){
			@Override
			public int compare(InventoryItem lhs, InventoryItem rhs){
				return lhs.customerAccountId.compareToIgnoreCase(rhs.customerAccountId);
			}
		},customerAccountAliasName(){
			@Override
			public int compare(InventoryItem lhs, InventoryItem rhs){
				return lhs.customerAccountAliasName.compareToIgnoreCase(rhs.customerAccountAliasName);
			}
		},locationAddressLine1(){
			@Override
			public int compare(InventoryItem lhs, InventoryItem rhs){
				Arrays.sort(lhs.getLocation(), InventoryLocation.Order.addressLine1);
				Arrays.sort(rhs.getLocation(), InventoryLocation.Order.addressLine1);
				if(lhs.getLocation().length > 0 && rhs.getLocation().length > 0){
					return lhs.getLocation()[0].getAddressLine1().compareToIgnoreCase(rhs.getLocation()[0].getAddressLine1());
				}else{
					return 0;
				}
				
			}
		},locationCity(){
			@Override
			public int compare(InventoryItem lhs, InventoryItem rhs){
				Arrays.sort(lhs.getLocation(), InventoryLocation.Order.city);
				Arrays.sort(rhs.getLocation(), InventoryLocation.Order.city);
				if(lhs.getLocation().length > 0 && rhs.getLocation().length > 0){
					return lhs.getLocation()[0].getCity().compareToIgnoreCase(rhs.getLocation()[0].getCity());
				}else{
					return 0;
				}
				
			}
		},locationPostalCode(){
			@Override
			public int compare(InventoryItem lhs, InventoryItem rhs){
				Arrays.sort(lhs.getLocation(), InventoryLocation.Order.postalCode);
				Arrays.sort(rhs.getLocation(), InventoryLocation.Order.postalCode);
				if(lhs.getLocation().length > 0 && rhs.getLocation().length > 0){
					return lhs.getLocation()[0].getPostalCode().compareToIgnoreCase(rhs.getLocation()[0].getPostalCode());
				}else{
					return 0;
				}
				
			}
		},locationStateProvinceCode(){
			@Override
			public int compare(InventoryItem lhs, InventoryItem rhs){
				Arrays.sort(lhs.getLocation(), InventoryLocation.Order.stateProvinceCode);
				Arrays.sort(rhs.getLocation(), InventoryLocation.Order.stateProvinceCode);
				if(lhs.getLocation().length > 0 && rhs.getLocation().length > 0){
					return lhs.getLocation()[0].getStateProvinceCode().compareToIgnoreCase(rhs.getLocation()[0].getStateProvinceCode());
				}else{
					return 0;
				}
				
			}
		},locationCountryCode(){
			@Override
			public int compare(InventoryItem lhs, InventoryItem rhs){
				Arrays.sort(lhs.getLocation(), InventoryLocation.Order.countryCode);
				Arrays.sort(rhs.getLocation(), InventoryLocation.Order.countryCode);
				if(lhs.getLocation().length > 0 && rhs.getLocation().length > 0){
					return lhs.getLocation()[0].getCountryCode().compareToIgnoreCase(rhs.getLocation()[0].getCountryCode());
				}else{
					return 0;
				}
				
			}
		};
		
		public abstract int compare(InventoryItem lhs, InventoryItem rhs);
		
		public Comparator<InventoryItem> ascending() {
			return this;     
		}
		
		public Comparator<InventoryItem> descending() {
			return Collections.reverseOrder(this);
		}
		
		public static Order fromValue(String v) {
            for (Order c: Order.values()) {
                if (c.toString().equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }
	}

	public Long getSeatCount() {
		return seatCount;
	}
	public void setSeatCount(Long seatCount) {
		this.seatCount = seatCount;
	}
	public Integer ComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	
}
