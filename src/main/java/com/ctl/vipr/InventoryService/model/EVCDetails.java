package com.ctl.vipr.InventoryService.model;

public class EVCDetails{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7843008262756471469L;
	private String id;
	private String customName;
	private String bandwidth; //(in MBPS)
	private String memberObjectId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getCustomName() {
		return customName;
	}
	public void setCustomName(String customName) {
		this.customName = customName;
	}
	public String getBandwidth() {
		return bandwidth;
	}
	public void setBandwidth(String bandwidth) {
		this.bandwidth = bandwidth;
	}
	public String getMemberObjectId() {
		return memberObjectId;
	}
	public void setMemberObjectId(String memberObjectId) {
		this.memberObjectId = memberObjectId;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EVCDetails [id=");
		builder.append(id);
		builder.append(", customName=");
		builder.append(customName);
		builder.append(", bandwidth=");
		builder.append(bandwidth);
		builder.append(", memberObjectId=");
		builder.append(memberObjectId);		
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
