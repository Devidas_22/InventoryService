package com.ctl.vipr.InventoryService.model;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/*
 *  author : jxredd5
 */

public class SearchFilterCondition implements Serializable{
	
	private static final long serialVersionUID = 149875234L;
	
	protected transient Logger log = LoggerFactory.getLogger(this.getClass());	
	
	public static final String EQ = "EQ";
	
	public static final String GT = "GT";
	
	public static final String LT = "LT";
	
	public static final String NE = "NE";
	
	public static final String IN = "IN";
	
	public static final String GE = "GE";
	
	public static final String LE = "LE";
	
	public String conditionalOperator = SearchFilterCondition.EQ;
	public SearchFilter filter = new SearchFilter();
	
public boolean ignoreCase = Boolean.TRUE;
	
	public boolean isIgnoreCase() {
		return ignoreCase;
	}

	public void setIgnoreCase(boolean ignoreCase) {
		this.ignoreCase = ignoreCase;
	}
	
	public SearchFilterCondition(){
	};
	
	public SearchFilterCondition(String condOperator, SearchFilter filter){
		this.conditionalOperator = condOperator;
		this.filter = filter;
	}
	
	/*
	 * This constructor is for forming a filterCondition from a simple filterString like Term:>value
	 */
	public SearchFilterCondition(String input){
		try{
			this.conditionalOperator = getConditionalOperator(input);
			this.filter = getSearchFilter(input);
		}catch(Exception e){
			log.error("Exception received while forming a filterCondition from a simple filterString- {}. And the Exception is {}", input, e.getMessage());
		}
		
	}

	public String getConditionalOperator() {
		return conditionalOperator;
	}

	public void setConditionalOperator(String conditionalOperator) {
		this.conditionalOperator = conditionalOperator;
	}

	public SearchFilter getFilter() {
		return filter;
	}

	public void setFilter(SearchFilter filter) {
		this.filter = filter;
	};
	
	/*
	 * This method is for finding the operator in a filterString.
	 * Example : input= serviceId:1234* output = :
	 * 			 input= bandwidth:>8Mbps output = :>			
	 */
	public static String getOperator(String filter){
		if(filter.indexOf(SearchOptions.CONDITIONAL_OPERATOR_NOT_EQUALS) != -1){
			return SearchOptions.CONDITIONAL_OPERATOR_NOT_EQUALS;
		}else if(filter.indexOf(SearchOptions.CONDITIONAL_OPERATOR_GREATER_THAN) != -1){
			return SearchOptions.CONDITIONAL_OPERATOR_GREATER_THAN;
		}else if(filter.indexOf(SearchOptions.CONDITIONAL_OPERATOR_LESS_THAN) != -1){
			return SearchOptions.CONDITIONAL_OPERATOR_LESS_THAN;
		}else if(filter.indexOf(SearchOptions.CONDITIONAL_OPERATOR_IN) != -1){
			return SearchOptions.CONDITIONAL_OPERATOR_IN;
		}else if(filter.indexOf(SearchOptions.CONDITIONAL_OPERATOR_LESS_THAN_OR_EQUAL) != -1){
			return SearchOptions.CONDITIONAL_OPERATOR_LESS_THAN_OR_EQUAL;
		}else if(filter.indexOf(SearchOptions.CONDITIONAL_OPERATOR_GREATER_THAN_OR_EQUAL) != -1){
			return SearchOptions.CONDITIONAL_OPERATOR_GREATER_THAN_OR_EQUAL;
		}else if(filter.indexOf(SearchOptions.CONDITIONAL_OPERATOR_EQUALS) != -1){
			return SearchOptions.CONDITIONAL_OPERATOR_EQUALS;
		}
		return null;
	}
	
	/*
	 * This method is to get the conditional operator to be set to the FilterExpressions
	 * Examples : input= serviceId:1234* output = EQ
	 * 			 input= bandwidth:>8Mbps output = GT
	 */
	public static String getConditionalOperator(String filter){
		if(filter.indexOf(SearchOptions.CONDITIONAL_OPERATOR_NOT_EQUALS) != -1){
			return NE;
		}else if(filter.indexOf(SearchOptions.CONDITIONAL_OPERATOR_GREATER_THAN) != -1){
			return GT;
		}else if(filter.indexOf(SearchOptions.CONDITIONAL_OPERATOR_LESS_THAN) != -1){
			return LT;
		}else if(filter.indexOf(SearchOptions.CONDITIONAL_OPERATOR_IN) != -1){
			return IN;
		}else if(filter.indexOf(SearchOptions.CONDITIONAL_OPERATOR_GREATER_THAN_OR_EQUAL) != -1){
			return GE;
		}else if(filter.indexOf(SearchOptions.CONDITIONAL_OPERATOR_LESS_THAN_OR_EQUAL) != -1){
			return LE;
		}else if(filter.indexOf(SearchOptions.CONDITIONAL_OPERATOR_EQUALS) != -1){
			return EQ;
		}
		return null;
	}
	
	public static SearchFilter getSearchFilter(String filter){
		if(filter == null) return null;
		String operator = getOperator(filter);
		if(operator == null) return null;
		int startIndex = filter.indexOf(operator);
		int endIndex =  (startIndex+operator.length())-1;
		return new SearchFilter(filter.substring(0,startIndex),filter.substring(endIndex+1));
	}
	
	public String toString(){
		return this.filter.getFilterName()+":"+this.conditionalOperator+":"+this.filter.getFilterValue();
	}
	
	/**
	 * toJoSqlString for a condition returns the string.
	 * Different datatypes like Number, Date and String are handled.
	 * For numbers, there will be no LIKE, it will = or !=
	 * For Date, the operators are restricted to < or > or =
	 * For String, the operators will be LIKE or NOTLIKE and each string will have a % preceded and another % succeeded.
	 * @return
	 */
	public String toJoSqlString(){
		if(isNumber(this.filter.getFilterValue()))
			return this.filter.getFilterName()+" "+toJoSqlOperator(this.conditionalOperator, this.filter.getFilterValue())+" "+this.filter.getFilterValue();
		else if(isDate(this.filter.getFilterValue()))
			return this.filter.getFilterName()+" "+toJoSqlOperator(this.conditionalOperator, this.filter.getFilterValue())+" '"+this.filter.getFilterValue().replaceAll("\\*", "")+"'";
		else{
			if(this.filter.getFilterName().equalsIgnoreCase("content"))
				return "upper("+this.filter.getFilterName()+") "+toJoSqlOperator(this.conditionalOperator, this.filter.getFilterValue())+" '%"+this.filter.getFilterValue().replaceAll("\\*", "")+"%'";
			else
				return this.filter.getFilterName()+" "+toJoSqlOperator(this.conditionalOperator, this.filter.getFilterValue())+" '%"+this.filter.getFilterValue().replaceAll("\\*", "")+"%'";
		}
			
	}
	
	
	/**
	 * A helper method to identify the joSql operator depending on the SearchOptions' OPERATOR
	 * @param operator
	 * @param value
	 * @return
	 */
	public String toJoSqlOperator(String operator, String value){
		String joSqlOperator = null;
			if(operator.equals("AND") || operator.equals("OR")){
				return operator;
			}else if(operator.equals(EQ)){
				if(isNumber(value))
					return "=";
				else
					return "LIKE";
			}else if(operator.equals(NE)){
				if(isNumber(value))
					return "!=";
				else
					return "NOT LIKE";
			}else if(operator.equals(GT)){
				return ">";
			}else if(operator.equals(LT)){
				return "<";
			}else if(operator.equals(IN)){
				return "#";
			}else if(operator.equals(GE)){
				return ">=";
			}else if(operator.equals(LE)){
				return "<=";
			}
		return joSqlOperator;
	}
	
	
	/**
	 * to check whether a passed string is a number or not
	 * @param value
	 * @return
	 */
	public boolean isNumber(String value){
		for(Character c : value.toCharArray()){
			if(!c.isDigit(c)){
				return false;
			}else if(this.filter.getFilterName().equals("tags") 
					|| this.filter.getFilterName().equals("keywords") || this.filter.getFilterName().equals("contentId")
					|| this.filter.getFilterName().equals("content")){
				return false;
			}else{
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * to check whether a passed string is a date or not. Frontend is restricted to send any dates 
	 * in the dd-MMM-yyyy format.
	 * @param value
	 * @return
	 */
	public boolean isDate(String value){
//		try{
//			if(StringUtils.getDateObjFromString(value, "dd-MMM-yyyy") != null)
//				return true;
//			else
//				return false;
//		}catch(Exception e){
//			return false;
//		}
		return false;
	}
}
