package com.ctl.vipr.InventoryService.model;

public class TollFreeFeature{
		 
    /**
	 * 
	 */
	private static final long serialVersionUID = 7156376627135497569L;
	java.lang.String featureCode;
	 java.lang.String featureDesc;
	 java.lang.Long featureId;
	 
	public java.lang.String getFeatureCode() {
		return featureCode;
	}
	public void setFeatureCode(java.lang.String featureCode) {
		this.featureCode = featureCode;
	}
	public java.lang.String getFeatureDesc() {
		return featureDesc;
	}
	public void setFeatureDesc(java.lang.String featureDesc) {
		this.featureDesc = featureDesc;
	}
	public java.lang.Long getFeatureId() {
		return featureId;
	}
	public void setFeatureId(java.lang.Long featureId) {
		this.featureId = featureId;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("QciTollFreeFeature [featureCode=");
		builder.append(featureCode);
		builder.append(", featureDesc=");
		builder.append(featureDesc);
		builder.append(", featureId=");
		builder.append(featureId);
		builder.append("]");
		return builder.toString();
	}
	
	
}

