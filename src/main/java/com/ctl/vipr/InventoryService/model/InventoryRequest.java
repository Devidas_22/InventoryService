package com.ctl.vipr.InventoryService.model;

import java.io.Serializable;

public class InventoryRequest implements Serializable {
	
	private String enterpriseId; 
	private String userId;
	private String searchString;
	private String pageInfo;
	private String sortInfo;
	
	/*
	 * To be used only for getEnterpriseInventory rest request call.
	 * Did not want to create a new pojo just for this. Hope these
	 * javadocs help clarify.
	 */
	private String[] productTypeCodeList;
	
	@Override
	public String toString() {
		return "InventoryRequest [enterpriseId=" + enterpriseId + ", userId="
				+ userId + ", searchString=" + searchString + ", pageInfo="
				+ pageInfo + ", sortInfo=" + sortInfo + "]";
	}
	public String getEnterpriseId() {
		return enterpriseId;
	}
	public void setEnterpriseId(String enterpriseId) {
		this.enterpriseId = enterpriseId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getSearchString() {
		return searchString;
	}
	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}
	public String getPageInfo() {
		return pageInfo;
	}
	public void setPageInfo(String pageInfo) {
		this.pageInfo = pageInfo;
	}
	public String getSortInfo() {
		return sortInfo;
	}
	public void setSortInfo(String sortInfo) {
		this.sortInfo = sortInfo;
	}
	public String[] getProductTypeCodeList() {
		return productTypeCodeList;
	}
	public void setProductTypeCodeList(String[] productTypeCodeList) {
		this.productTypeCodeList = productTypeCodeList;
	}
		

}
