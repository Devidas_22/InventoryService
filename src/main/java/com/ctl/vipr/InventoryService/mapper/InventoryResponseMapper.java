package com.ctl.vipr.InventoryService.mapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ctl.ecaps.serviceobject.AtmInventorySO;
import com.ctl.ecaps.serviceobject.CeInventorySO;
import com.ctl.ecaps.serviceobject.CugSO;
import com.ctl.ecaps.serviceobject.DedicatedLdInventorySO;
import com.ctl.ecaps.serviceobject.DiaInventorySO;
import com.ctl.ecaps.serviceobject.DplInventorySO;
import com.ctl.ecaps.serviceobject.ElineInventorySO;
import com.ctl.ecaps.serviceobject.EplInventorySO;
import com.ctl.ecaps.serviceobject.EspInventorySO;
import com.ctl.ecaps.serviceobject.EvcMemberSO;
import com.ctl.ecaps.serviceobject.FrameInventorySO;
import com.ctl.ecaps.serviceobject.InventoryLocationSO;
import com.ctl.ecaps.serviceobject.InventoryResponseSO;
import com.ctl.ecaps.serviceobject.InventorySO;
import com.ctl.ecaps.serviceobject.IplInventorySO;
import com.ctl.ecaps.serviceobject.IqInventorySO;
import com.ctl.ecaps.serviceobject.LdInventorySO;
import com.ctl.ecaps.serviceobject.LsInventorySO;
import com.ctl.ecaps.serviceobject.MfwVpnInventorySO;
import com.ctl.ecaps.serviceobject.MoeInventorySO;
import com.ctl.ecaps.serviceobject.NbsInventorySO;
import com.ctl.ecaps.serviceobject.NmInventorySO;
import com.ctl.ecaps.serviceobject.OwsInventorySO;
import com.ctl.ecaps.serviceobject.PlInventorySO;
import com.ctl.ecaps.serviceobject.RelatedInventorySO;
import com.ctl.ecaps.serviceobject.TollFreeFeatureSO;
import com.ctl.ecaps.serviceobject.TollFreeInventorySO;
import com.ctl.ecaps.serviceobject.VoipInventorySO;
import com.ctl.vipr.InventoryService.model.CUGDetails;
import com.ctl.vipr.InventoryService.model.DAResponse;
import com.ctl.vipr.InventoryService.model.EVCDetails;
import com.ctl.vipr.InventoryService.model.InventoryItem;
import com.ctl.vipr.InventoryService.model.InventoryLocation;
import com.ctl.vipr.InventoryService.model.InventoryRequest;
import com.ctl.vipr.InventoryService.model.InventoryResponse;
import com.ctl.vipr.InventoryService.model.RelatedInventory;
import com.ctl.vipr.InventoryService.model.TollFreeFeature;

public class InventoryResponseMapper {

//	private Cache<String, Object> cache;
 
	private static Logger log = LoggerFactory.getLogger(InventoryResponseMapper.class);

	public InventoryResponse mapResponse(InventoryResponseSO interfaceResponse, InventoryRequest requestObj) {
		DAResponse response = new DAResponse();

		if(interfaceResponse != null){
			return	mapQciResponse("getInventory",interfaceResponse,requestObj,response);

		}
		
		return null;
	}

	protected InventoryResponse mapQciResponse(String request, InventoryResponseSO data,InventoryRequest requestObj,DAResponse res){
		InventoryResponse response = new InventoryResponse(res);
		String pageInfo = requestObj.getPageInfo();
		List<InventorySO> qcinventory = data.getInventory();
		response.setTotalNumberOfRecords(data.getTotalRowCount());
		response.setNumberOfRecords(data.getRowCount());
		response.setEnterpriseId(requestObj.getEnterpriseId());
		if(log.isInfoEnabled())
			log.info("Size of inventory from interface : {} ", data.getRowCount());
		ArrayList<InventoryItem> qcinventoryList = getInventory(request, qcinventory, response, pageInfo);
		if (null!=qcinventoryList && qcinventoryList.size() > 0) {			 
			InventoryItem[] qcInvArray = new InventoryItem[qcinventoryList.size()];
			response.setInventory(qcinventoryList.toArray(qcInvArray));
		}
		return response;

	}

	protected ArrayList<InventoryItem> getInventory(String request, List<InventorySO> qcinventory, InventoryResponse response, String pageInfo){
		ArrayList<InventoryItem> qcinventoryList = new ArrayList<InventoryItem>();
		//Random random = new Random();
		Random random = new Random(System.currentTimeMillis());
		String entId = response.getEnterpriseId();
		
		if(null!=qcinventory && qcinventory.size()>0){
			for(int i=0;i<qcinventory.size();i++) {
				InventoryItem qcinventoryObj=new InventoryItem();
			//	qcinventoryObj.setId(new Double(random.nextDouble()).toString());
				qcinventoryObj.setId(new Long(random.nextLong()).toString());
				qcinventoryObj.setEnterpriseId(entId);
//				qcinventoryObj.setSrc(VnomStatus.INVENTORY_RESPONSE_QCTRL);
				qcinventoryObj = mapCommonFields(qcinventoryObj, qcinventory.get(i), entId);
				if(qcinventory.get(i) instanceof IqInventorySO){
					qcinventoryObj = mapIqSpecificFields(qcinventory.get(i), qcinventoryObj, entId);
				}else if(qcinventory.get(i) instanceof CeInventorySO){
					qcinventoryObj = mapCeSpecificFields(qcinventory.get(i), qcinventoryObj, entId);
				}else if(qcinventory.get(i) instanceof VoipInventorySO){
					qcinventoryObj = mapVoipSpecificFields(qcinventory.get(i), qcinventoryObj, entId);
				}else if(qcinventory.get(i) instanceof DedicatedLdInventorySO){
					qcinventoryObj = mapDedLdSpecificFields(qcinventory.get(i), qcinventoryObj, entId);
				}else if(qcinventory.get(i) instanceof MoeInventorySO){
					qcinventoryObj = mapMoeSpecificFields(qcinventory.get(i), qcinventoryObj, entId);
				}else if(qcinventory.get(i) instanceof NmInventorySO){
					qcinventoryObj = mapNmSpecificFields(qcinventory.get(i), qcinventoryObj, entId);
				}else if(qcinventory.get(i) instanceof PlInventorySO){
					qcinventoryObj = mapPlSpecificFields(qcinventory.get(i), qcinventoryObj, entId);
				}else if(qcinventory.get(i) instanceof EplInventorySO){
					qcinventoryObj = mapEplSpecificFields(qcinventory.get(i), qcinventoryObj, entId);
				}else if(qcinventory.get(i) instanceof DplInventorySO){
					qcinventoryObj = mapDplSpecificFields(qcinventory.get(i), qcinventoryObj, entId);
				}else if(qcinventory.get(i) instanceof OwsInventorySO){
					qcinventoryObj = mapOwsSpecificFields(qcinventory.get(i), qcinventoryObj, entId);
				}else if(qcinventory.get(i) instanceof IplInventorySO){
					qcinventoryObj = mapIplSpecificFields(qcinventory.get(i), qcinventoryObj, entId);
				}else if(qcinventory.get(i) instanceof LdInventorySO){
					qcinventoryObj = mapLdSpecificFields(qcinventory.get(i), qcinventoryObj, entId);
				}else if(qcinventory.get(i) instanceof TollFreeInventorySO){
					qcinventoryObj = mapTfSpecificFields(qcinventory.get(i), qcinventoryObj, entId);
				}else if(qcinventory.get(i) instanceof EspInventorySO){
					qcinventoryObj = mapEspSpecificFields(qcinventory.get(i), qcinventoryObj, entId);
				}else if(qcinventory.get(i) instanceof NbsInventorySO){
					qcinventoryObj = mapNbsInvSpecificFields(qcinventory.get(i), qcinventoryObj, entId);
				}else if(qcinventory.get(i) instanceof ElineInventorySO){
					qcinventoryObj = mapElineSpecificFields(qcinventory.get(i), qcinventoryObj, entId);
				}else if(qcinventory.get(i) instanceof AtmInventorySO){
					qcinventoryObj = mapAtmSpecificFields(qcinventory.get(i), qcinventoryObj, entId);
				}else if(qcinventory.get(i) instanceof FrameInventorySO){
					qcinventoryObj = mapFrameSpecificFields(qcinventory.get(i), qcinventoryObj, entId);
				}else if(qcinventory.get(i) instanceof LsInventorySO){
						qcinventoryObj = mapLssSpecificFields(qcinventory.get(i), qcinventoryObj, entId);
				}else if(qcinventory.get(i) instanceof DiaInventorySO){
					qcinventoryObj = mapDiaSpecificFields(qcinventory.get(i), qcinventoryObj, entId);
				}else if(qcinventory.get(i) instanceof MfwVpnInventorySO){
					qcinventoryObj = mapMfwVpnSpecificFields(qcinventory.get(i), qcinventoryObj, entId);
				}
				qcinventoryList.add(qcinventoryObj);
				
			}
		}

		return qcinventoryList;
	}


	private InventoryItem mapLssSpecificFields(InventorySO inventorySO,
			InventoryItem qcinventoryObj, String entId) {
		qcinventoryObj.setLegalEnityCode("QC");
		return qcinventoryObj;
	}

	protected InventoryItem mapElineSpecificFields(InventorySO inventorySo, InventoryItem qcinventoryObj, String entId){
		ElineInventorySO elineInvSO = (ElineInventorySO)inventorySo;

		qcinventoryObj.setBandwidth(elineInvSO.getBandwidth());
		qcinventoryObj.setServiceElementId(elineInvSO.getServiceElementId());
		qcinventoryObj.setServiceCategoryId(elineInvSO.getServiceCategoryId());
		return qcinventoryObj;
	}


	protected InventoryItem mapAtmSpecificFields(InventorySO inventorySo, InventoryItem qcinventoryObj, String entId){
		AtmInventorySO atmInvSO = (AtmInventorySO)inventorySo;
		qcinventoryObj.setBandwidth(atmInvSO.getBandwidth());
		if(qcinventoryObj.getAccountSystemCode()!= null && (qcinventoryObj.getAccountSystemCode().equalsIgnoreCase("CABS") || qcinventoryObj.getAccountSystemCode().equalsIgnoreCase("IABS"))){
			qcinventoryObj.setLegalEnityCode("QC");
		}
		return qcinventoryObj;
	}


	protected InventoryItem mapFrameSpecificFields(InventorySO inventorySo, InventoryItem qcinventoryObj, String entId){
		FrameInventorySO frameInvSO = (FrameInventorySO)inventorySo;
		qcinventoryObj.setBandwidth(frameInvSO.getBandwidth());
		if(qcinventoryObj.getAccountSystemCode()!= null && (qcinventoryObj.getAccountSystemCode().equalsIgnoreCase("CABS") || qcinventoryObj.getAccountSystemCode().equalsIgnoreCase("IABS"))){
			qcinventoryObj.setLegalEnityCode("QC");
		}
		return qcinventoryObj;
	}

	protected InventoryItem mapNbsInvSpecificFields(InventorySO inventorySo, InventoryItem qcinventoryObj, String entId){

		NbsInventorySO nbsInventorySO = (NbsInventorySO)inventorySo;
		qcinventoryObj.setLimsStatusCode(nbsInventorySO.getLimsStatusCode());
		qcinventoryObj.setBandwidth(nbsInventorySO.getBandwidth());
		qcinventoryObj.setServiceId(nbsInventorySO.getServiceId());
		qcinventoryObj.setServiceTypeCode(nbsInventorySO.getServiceTypeCode());
		return qcinventoryObj;
	}



	protected InventoryItem mapEspSpecificFields(InventorySO inventorySo, InventoryItem qcinventoryObj, String entId){
		EspInventorySO espInventorySO=(EspInventorySO)inventorySo;
		qcinventoryObj.setAvailableSessions(espInventorySO.getAvailableSessions());
		qcinventoryObj.setTotalSessions(espInventorySO.getTotalSessions());
		qcinventoryObj.setSipSwitchId(espInventorySO.getSipSwitchId());

		qcinventoryObj.setPoolType(espInventorySO.getServiceTypeCode());

		qcinventoryObj.setServiceName(espInventorySO.getServiceName());
		return qcinventoryObj;
	}


	protected InventoryItem mapIqSpecificFields(InventorySO inventorySo, InventoryItem qcinventoryObj, String entId){


		IqInventorySO iqInventorySO = (IqInventorySO)inventorySo;
		qcinventoryObj.setBandwidth(iqInventorySO.getBandwidth());
		qcinventoryObj.setServiceTypeCode(iqInventorySO.getServiceTypeCode());
		qcinventoryObj.setLimsStatusCode(iqInventorySO.getLimsStatusCode());
		qcinventoryObj.setDetailsDataAvailable(iqInventorySO.isDetailDataAvailable());
		return qcinventoryObj;
	}
	
	protected InventoryItem mapCeSpecificFields(InventorySO ceventorySo, InventoryItem qcinventoryObj, String entId){

		CeInventorySO ceInventorySO = (CeInventorySO)ceventorySo;
		qcinventoryObj.setBandwidth(ceInventorySO.getBandwidth());
		return qcinventoryObj;
	}

	protected InventoryItem mapVoipSpecificFields(InventorySO inventorySo, InventoryItem qcinventoryObj, String entId){
		VoipInventorySO voipInventorySO=(VoipInventorySO)inventorySo;
		qcinventoryObj.setTenantId(voipInventorySO.getTenantId()); 
		qcinventoryObj.setServiceTypeCode(voipInventorySO.getServiceTypeCode());
		qcinventoryObj.setServiceElementId(voipInventorySO.getServiceElementId());
		qcinventoryObj.setServiceCategoryId(voipInventorySO.getServiceCategoryId());
		return qcinventoryObj;
	}

	protected InventoryItem mapDedLdSpecificFields(InventorySO inventorySo, InventoryItem qcinventoryObj, String entId){
		DedicatedLdInventorySO dedicatedLdInventorySO=(DedicatedLdInventorySO)inventorySo;
		qcinventoryObj.setSwitchId(dedicatedLdInventorySO.getSwitchId());
		qcinventoryObj.setNumericCircuitId(dedicatedLdInventorySO.getNumericCircuitId());
		return qcinventoryObj;
	}

	protected InventoryItem mapLdSpecificFields(InventorySO inventorySo, InventoryItem qcinventoryObj, String entId){
		LdInventorySO ldInventorySO=(LdInventorySO)inventorySo;
		qcinventoryObj.setComponentGroupCode(ldInventorySO.getComponentGroupCode());
		qcinventoryObj.setComponentGroupValue(ldInventorySO.getComponentGroupValue());
		return qcinventoryObj;
	}

	protected InventoryItem mapPlSpecificFields(InventorySO inventorySO, InventoryItem qcinventoryObj, String entId){
		PlInventorySO plInventorySO = (PlInventorySO)inventorySO;
		qcinventoryObj.setBandwidth(plInventorySO.getBandwidth());
		qcinventoryObj.setDiversityIndicator(plInventorySO.isDiversityIndicator());
		qcinventoryObj.setServiceElementId(plInventorySO.getServiceElementId());
		qcinventoryObj.setServiceCategoryId(plInventorySO.getServiceCategoryId());
		qcinventoryObj.setDetailsDataAvailable(plInventorySO.isDetailDataAvailable());
		qcinventoryObj.setTirksCircuitId(plInventorySO.getTirksCircuitId());
		qcinventoryObj.setCircuitFormat(plInventorySO.getCircuitFormat());
		qcinventoryObj.setProductAccountId(plInventorySO.getProductAccountId());
		if(plInventorySO.getInstallationDate()!=null ){
			qcinventoryObj.setInstallationDate(plInventorySO.getInstallationDate().toString());
		}
		if(plInventorySO.getContractTerminationDate() != null){
			qcinventoryObj.setContractTerminationDate(plInventorySO.getContractTerminationDate().toString());
		}
		return qcinventoryObj;
	}

	protected InventoryItem mapEplSpecificFields(InventorySO inventorySO, InventoryItem qcinventoryObj, String entId){
		EplInventorySO eplInvSO = (EplInventorySO)inventorySO;
		qcinventoryObj.setBandwidth(eplInvSO.getBandwidth());
		qcinventoryObj.setDiversityIndicator(eplInvSO.isDiversityIndicator());
		qcinventoryObj.setServiceElementId(eplInvSO.getServiceElementId());
		qcinventoryObj.setServiceCategoryId(eplInvSO.getServiceCategoryId());
		qcinventoryObj.setDetailsDataAvailable(eplInvSO.isDetailDataAvailable());
		/*
		 * JXREDD5: tirksCircuitId mapping below added as per the request from UI team, as they want to use this inventory for 
		 * create ticket.
		 * Same mapping repeated for all the pl types below.
		 */
		qcinventoryObj.setTirksCircuitId(eplInvSO.getTirksCircuitId());
		qcinventoryObj.setCircuitFormat(eplInvSO.getCircuitFormat());
		if(eplInvSO.getInstallationDate()!=null ){
			qcinventoryObj.setInstallationDate(eplInvSO.getInstallationDate().toString());
		}
		if(eplInvSO.getContractTerminationDate() != null){
			qcinventoryObj.setContractTerminationDate(eplInvSO.getContractTerminationDate().toString());
		}
		return qcinventoryObj;

	}

	protected InventoryItem mapDplSpecificFields(InventorySO inventorySO, InventoryItem qcinventoryObj, String entId){
		DplInventorySO dplInventorySO = (DplInventorySO)inventorySO;
		qcinventoryObj.setBandwidth(dplInventorySO.getBandwidth());
		qcinventoryObj.setDiversityIndicator(dplInventorySO.isDiversityIndicator());
		qcinventoryObj.setServiceElementId(dplInventorySO.getServiceElementId());
		qcinventoryObj.setServiceCategoryId(dplInventorySO.getServiceCategoryId());
		qcinventoryObj.setDetailsDataAvailable(dplInventorySO.isDetailDataAvailable());
		qcinventoryObj.setTirksCircuitId(dplInventorySO.getTirksCircuitId());
		qcinventoryObj.setCircuitFormat(dplInventorySO.getCircuitFormat());
		if(dplInventorySO.getInstallationDate()!=null ){
			qcinventoryObj.setInstallationDate(dplInventorySO.getInstallationDate().toString());
		}
		if(dplInventorySO.getContractTerminationDate() != null){
			qcinventoryObj.setContractTerminationDate(dplInventorySO.getContractTerminationDate().toString());
		}
		return qcinventoryObj;
	}


	protected InventoryItem mapOwsSpecificFields(InventorySO inventorySO, InventoryItem qcinventoryObj, String entId){
		OwsInventorySO owsInventorySO = (OwsInventorySO)inventorySO;
		qcinventoryObj.setBandwidth(owsInventorySO.getBandwidth());
		qcinventoryObj.setDiversityIndicator(owsInventorySO.isDiversityIndicator());
		qcinventoryObj.setServiceElementId(owsInventorySO.getServiceElementId());
		qcinventoryObj.setServiceCategoryId(owsInventorySO.getServiceCategoryId());
		qcinventoryObj.setDetailsDataAvailable(owsInventorySO.isDetailDataAvailable());
		qcinventoryObj.setTirksCircuitId(owsInventorySO.getTirksCircuitId());
		qcinventoryObj.setCircuitFormat(owsInventorySO.getCircuitFormat());
		if(owsInventorySO.getInstallationDate()!=null ){
			qcinventoryObj.setInstallationDate(owsInventorySO.getInstallationDate().toString());
		}
		if(owsInventorySO.getContractTerminationDate() != null){
			qcinventoryObj.setContractTerminationDate(owsInventorySO.getContractTerminationDate().toString());
		}
		return qcinventoryObj;
	}


	protected InventoryItem mapIplSpecificFields(InventorySO inventorySO, InventoryItem qcinventoryObj, String entId){
		IplInventorySO iplInventorySO = (IplInventorySO)inventorySO;
		qcinventoryObj.setBandwidth(iplInventorySO.getBandwidth());
		qcinventoryObj.setDiversityIndicator(iplInventorySO.isDiversityIndicator());
		qcinventoryObj.setServiceElementId(iplInventorySO.getServiceElementId());
		qcinventoryObj.setServiceCategoryId(iplInventorySO.getServiceCategoryId());
		qcinventoryObj.setDetailsDataAvailable(iplInventorySO.isDetailDataAvailable());
		qcinventoryObj.setTirksCircuitId(iplInventorySO.getTirksCircuitId());
		qcinventoryObj.setCircuitFormat(iplInventorySO.getCircuitFormat());
		if(iplInventorySO.getInstallationDate()!=null ){
			qcinventoryObj.setInstallationDate(iplInventorySO.getInstallationDate().toString());
		}
		if(iplInventorySO.getContractTerminationDate() != null){
			qcinventoryObj.setContractTerminationDate(iplInventorySO.getContractTerminationDate().toString());
		}
		return qcinventoryObj;
	}

	protected InventoryItem mapNmSpecificFields(InventorySO inventorySo, InventoryItem qcinventoryObj, String entId){
		NmInventorySO nmInventorySO = (NmInventorySO)inventorySo;
		qcinventoryObj.setServiceElementId(nmInventorySO.getServiceElementId());
		qcinventoryObj.setServiceCategoryId(nmInventorySO.getServiceCategoryId());
		qcinventoryObj.setRealIpV6(nmInventorySO.getRealIpV6());
		qcinventoryObj.setRealIp(nmInventorySO.getRealIp());
//		qcinventoryObj.setManagedIpV6(nmInventorySO.getManagedIpV6());
		qcinventoryObj.setManagedIp(nmInventorySO.getManagedIp());
		return qcinventoryObj;
	}

	protected InventoryItem mapMoeSpecificFields(InventorySO inventorySo, InventoryItem qcinventoryObj, String entId){
		List<EVCDetails> evcDetailsList = new ArrayList<EVCDetails>();
		EVCDetails evcDetails = null;
		MoeInventorySO moeInvSo = (MoeInventorySO)inventorySo;
		if(moeInvSo.getEvcMembers()!=null ){
			if(moeInvSo.getEvcMembers().size()>0){
				List<EvcMemberSO> evcMembers = moeInvSo.getEvcMembers();
				//				EvcMemberSO[] evcMembers = new EvcMemberSO[moeInvSo.getEvcMembers().size()] ;			    
				//				evcMembers = moeInvSo.getEvcMembers();	 
				if(evcMembers !=null && evcMembers.size()>0){
					for(EvcMemberSO evcMember : evcMembers){
						evcDetails = new EVCDetails();
						
						//Mapping QCTRL evcName to ID because QCTRL is giving id in evcName 
						evcDetails.setId(evcMember.getEvcId());
						//Mapping evcMemberAliasName to CustomName
						evcDetails.setCustomName(evcMember.getEvcMemberAliasName());
						evcDetails.setBandwidth(evcMember.getEvcMemberBandwidth());
						evcDetails.setMemberObjectId(evcMember.getEvcMemberId());
						//Mapping evcId to ObjectId
						//evcDetails.setObjectId(evcMember.getEvcId());
						evcDetailsList.add(evcDetails);
					}
				}
			}
		}		    
		qcinventoryObj.setEvcDetailsList(evcDetailsList);
		qcinventoryObj.setBandwidth((moeInvSo.getBandwidth()));
		qcinventoryObj.setDiversityIndicator(moeInvSo.isDiversityIndicator());
		qcinventoryObj.setQosIndicator(moeInvSo.isQosIndicator());
		qcinventoryObj.setBansAccountNumber(moeInvSo.getAccountId());
		qcinventoryObj.setLegalEnityCode("QC");
		return qcinventoryObj;
	}

	/*
	 * ARAMINE : Removed common fields from QciInventory POJO and can access them from InventoryItem POJO from VnomLibApi. 
	 * This change is to achieve common InventoryModel as for FEB 2014 release .	
	 * ProductAccountId which is Long ,is now changed to String in mapping while sending across to UI
	 */
	protected InventoryItem mapTfSpecificFields(InventorySO inventorySo, InventoryItem qcinventoryObj, String entId){
		TollFreeInventorySO tollFreeSummarySO = (TollFreeInventorySO)inventorySo;
		qcinventoryObj.setProductAccountId(tollFreeSummarySO.getProductAccountId());
		qcinventoryObj.setCallPlan(tollFreeSummarySO.getCallPlanDesc());
		if(tollFreeSummarySO.getRingToNumber() != null)
			qcinventoryObj.setTermination_ringTo(tollFreeSummarySO.getRingToNumber());
		else if(tollFreeSummarySO.getTrunkGroupName()!=null && tollFreeSummarySO.getDnisNumber()!=null) 
			qcinventoryObj.setTermination_ringTo(tollFreeSummarySO.getTrunkGroupName()  +"-"+  tollFreeSummarySO.getDnisNumber());
		else if(tollFreeSummarySO.getDnisNumber()==null)
			qcinventoryObj.setTermination_ringTo(tollFreeSummarySO.getTrunkGroupName());
		else if(tollFreeSummarySO.getTrunkGroupName()==null)
			qcinventoryObj.setTermination_ringTo(tollFreeSummarySO.getDnisNumber());					

		qcinventoryObj.setTermination_dnisName(tollFreeSummarySO.getDnisNumber());
		qcinventoryObj.setTermination_trunkGroup(tollFreeSummarySO.getTrunkGroupName());
		qcinventoryObj.setGroup_callPlan_cd(tollFreeSummarySO.getSharedCallPlanCode());
		qcinventoryObj.setGroup_callPlan_desc(tollFreeSummarySO.getSharedCallPlanValue());
		qcinventoryObj.setServiceId_name(tollFreeSummarySO.getTollFreeDesc());

		List<TollFreeFeatureSO> featureSO =((TollFreeInventorySO)inventorySo).getFeatures();
		List<TollFreeFeature> featureList = new ArrayList<TollFreeFeature>();
		if(featureSO != null && featureSO.size() >0){
			HashMap map = new HashMap();
			for(TollFreeFeatureSO tfFeatureSO : featureSO){
				String featureCode = tfFeatureSO.getFeatureDisplayCode();
				if(!tfFeatureSO.getFeatureDisplayCode().equalsIgnoreCase("NA") && !map.containsKey(featureCode)
						&& tfFeatureSO.getFeatureDisplayCode() != null && tfFeatureSO.getFeatureDisplayName() != null
						&& !tfFeatureSO.getFeatureDisplayName().equals("") && !tfFeatureSO.getFeatureDisplayCode().equals("")){
					map.put(featureCode, tfFeatureSO);
					TollFreeFeature feature = new TollFreeFeature();
					feature.setFeatureDesc(tfFeatureSO.getFeatureDisplayName());
					feature.setFeatureCode(tfFeatureSO.getFeatureDisplayCode());
					featureList.add(feature);
				}
			}
			map=null;
			qcinventoryObj.setTollFreeFeatures(featureList);
		}

		return qcinventoryObj;
	}

	protected InventoryItem mapDiaSpecificFields(InventorySO inventorySo, InventoryItem qcinventoryObj, String entId) {
		DiaInventorySO diaInvSO = (DiaInventorySO) inventorySo;
		qcinventoryObj.setComponentName(diaInvSO.getServiceAliasName());
		Long compId=Long.valueOf(diaInvSO.getComponentId());
		Integer cmpId = compId!=null ? compId.intValue() : null;
		qcinventoryObj.setComponentId(cmpId);
		/*qcinventoryObj.setLocation(createLocation(inventorySo.getLocations(),
				qcinventoryObj));*/

		return qcinventoryObj;
	}
	
	protected InventoryItem mapMfwVpnSpecificFields(InventorySO inventorySo, InventoryItem qcinventoryObj, String entId) {
		MfwVpnInventorySO mfwVpnInvSO = (MfwVpnInventorySO) inventorySo;
		qcinventoryObj.setComponentName(mfwVpnInvSO.getComponentName());
		qcinventoryObj.setServiceAliasName(mfwVpnInvSO.getComponentName());
		Long compId=Long.valueOf(mfwVpnInvSO.getComponentId());
		Integer cmpId = compId!=0 ? compId.intValue() : null;
		qcinventoryObj.setComponentId(cmpId);
		qcinventoryObj.setStatusCode(mfwVpnInvSO.getStatusCode());
		return qcinventoryObj;
	}

	/*protected InventoryLocation[] createLocation(
			List<InventoryLocationSO> inventoryLocationSO,
			InventoryItem qcinventoryObj) {
		List<InventoryLocation> locationsList = new ArrayList<InventoryLocation>();
		if (inventoryLocationSO != null && inventoryLocationSO.size() > 0) {
			for (InventoryLocationSO locationSO : inventoryLocationSO) {
				if (locationSO != null) {
					InventoryLocation location = new InventoryLocation();
					qcinventoryObj.setCity(locationSO.getCity());
					qcinventoryObj.setState(locationSO.getStateProvinceCode());
					qcinventoryObj.setZip(locationSO.getPostalCode());
					locationsList.add(location);

				}
			}
		}

		return locationsList
				.toArray(new InventoryLocation[locationsList.size()]);
	}*/

	protected InventoryItem mapCommonFields(InventoryItem qcinventoryObj, InventorySO inventorySO, String entId){
		qcinventoryObj.setServiceId(inventorySO.getServiceId());

		qcinventoryObj.setEnterpriseId(entId);

		// mapping for locations
		qcinventoryObj.setLocation(createInventoryLocation(inventorySO.getLocations()));	
		qcinventoryObj.setParentInventory(mapRelatedInventory(inventorySO.getParentInventory()));
		qcinventoryObj.setChildInventory(mapRelatedInventory(inventorySO.getChildInventory()));
		qcinventoryObj.setCustomerAccountId(inventorySO.getAccountId());
		qcinventoryObj.setCustomerAccountAliasName(inventorySO.getAccountAliasName());
		qcinventoryObj.setInventoryTypeCode(inventorySO.getInventoryTypeCode());
		qcinventoryObj.setServiceElementId(inventorySO.getServiceElementId());
		qcinventoryObj.setProductTypeCode(inventorySO.getProductTypeCode());
		qcinventoryObj.setServiceTypeCode(inventorySO.getServiceTypeCode());
		qcinventoryObj.setServiceAliasName(inventorySO.getServiceAliasName());
		qcinventoryObj.setServiceName(inventorySO.getServiceName());
		if(inventorySO.getInstallationDate() != null){
			qcinventoryObj.setInstallationDate(inventorySO.getInstallationDate().toString());
		}
		qcinventoryObj.setInventoryId(inventorySO.getInventoryId());
		if(inventorySO.getUbi()!=null)
		{
		qcinventoryObj.setUbi(inventorySO.getUbi().toString());
		}
		qcinventoryObj.setCustomerAccountName(inventorySO.getAccountName());
		if(inventorySO.getApplianceName() != null)
		{
			qcinventoryObj.setApplianceName(inventorySO.getApplianceName().toString());
		}

		if(inventorySO.getCugs() != null && inventorySO.getCugs().size() > 0){
			List<CugSO> cugSOList = inventorySO.getCugs();	
			List<CUGDetails> cugList = new ArrayList<CUGDetails>();	
			CUGDetails[] cugArr = new CUGDetails[cugSOList.size()];		
			for(CugSO cugSO : cugSOList){				
				cugList.add(mapCugDetails(cugSO));				
			}
			qcinventoryObj.setCugs(cugList.toArray(cugArr));
		}
		qcinventoryObj.setProductFamilyCode(inventorySO.getProductFamilyCode());
		qcinventoryObj.setAccountSystemCode(inventorySO.getAccountSystemCode());			
		qcinventoryObj.setProductFamilyName(inventorySO.getProductFamilyName());
		qcinventoryObj.setProductAccountId(inventorySO.getProductAccountId());
		qcinventoryObj.setLegalEnityCode("QCC");
		setParentMSIndicator(qcinventoryObj);
		return qcinventoryObj;
	}

	protected RelatedInventory[] mapRelatedInventory(List<RelatedInventorySO> relatedInventorySOList){
		List<RelatedInventory> relatedInventoryList = new ArrayList<RelatedInventory>();
		if(relatedInventorySOList != null && relatedInventorySOList.size()>0){
			for(RelatedInventorySO relatedInventorySO:relatedInventorySOList){
				if(relatedInventorySO != null) {
					RelatedInventory relatedInventory = new RelatedInventory();
					relatedInventory.setAccountId(relatedInventorySO.getAccountId());
					relatedInventory.setAccountAliasName(relatedInventorySO.getAccountAliasName());
					relatedInventory.setAccountSystemCd(relatedInventorySO.getAccountSystemCd());
					relatedInventory.setInventoryId(relatedInventorySO.getInventoryId());
					relatedInventory.setInventoryTypeCode(relatedInventorySO.getInventoryTypeCode());
					relatedInventory.setProductTypeCode(relatedInventorySO.getProductTypeCode());
					relatedInventory.setServiceAliasName(relatedInventorySO.getServiceAliasName());
					relatedInventory.setServiceId(relatedInventorySO.getServiceId());
					relatedInventory.setServiceTypeCode(relatedInventorySO.getServiceTypeCode());
					if(relatedInventorySO.getUbi()!=null)
					{
					relatedInventory.setUbi(relatedInventorySO.getUbi().toString());
					}
					if(null != relatedInventorySO.getSeatCount())
					    relatedInventory.setSeatCount(relatedInventorySO.getSeatCount());
					else
						relatedInventory.setSeatCount(0L);
					relatedInventoryList.add(relatedInventory);
				}
			}
		}
		return relatedInventoryList.toArray(new RelatedInventory[relatedInventoryList.size()]);
	}

	protected InventoryLocation[] createInventoryLocation(List<InventoryLocationSO> inventoryLocationSO) {
		List<InventoryLocation> locationsList = new ArrayList<InventoryLocation>();
		if(inventoryLocationSO !=null && inventoryLocationSO.size()>0){
			for(InventoryLocationSO locationSO:inventoryLocationSO){
				if(locationSO!=null){
					InventoryLocation location = new InventoryLocation();
					location.setCity(locationSO.getCity());
					location.setLocationId(locationSO.getLocationId());
					location.setPostalCode(locationSO.getPostalCode());
					location.setStateProvinceCode(locationSO.getStateProvinceCode());
					location.setLocationName(locationSO.getLocationName());
					location.setAddressLine1(locationSO.getAddressLine1());
					location.setAddressLine2(locationSO.getAddressLine2());
					location.setAddressLine3(locationSO.getAddressLine3());		
					location.setServiceAddressId(locationSO.getServiceAddressId());
					location.setCountryCode(locationSO.getCountryCode());
					location.setLocationTypeCode(locationSO.getLocationTypeCode());
					location.setClliCode(locationSO.getClliCode());
					location.setInventory(mapRelatedInventory(locationSO.getInventory()));
					if(null != locationSO.getSeatCount())
					    location.setSeatCount(locationSO.getSeatCount()); // Amrutha - CR4513
					else
						location.setSeatCount(0L);
					locationsList.add(location);

				}
			}
		}

		return locationsList.toArray(new InventoryLocation[locationsList.size()]);
	}

	protected CUGDetails mapCugDetails(CugSO cugSO){
		CUGDetails cugDetail = new CUGDetails();
		cugDetail.setCugName(cugSO.getCugName());
		cugDetail.setCugAliasName(cugSO.getCugAliasName());
		cugDetail.setCugId(cugSO.getCugId());
		cugDetail.setCugOwnerId(cugSO.getCugOwnerId());
		cugDetail.setCugOwnerName(cugSO.getCugOwnerName());
		cugDetail.setExtranetInd(cugSO.getExtranetInd());
		cugDetail.setNetworkName(cugSO.getNetworkName());
		return cugDetail;
	}

	private void setParentMSIndicator(InventoryItem invItem) {
		for(RelatedInventory relInv : invItem.getParentInventory()){
			if(relInv.getProductTypeCode().equalsIgnoreCase("MS")){
				invItem.setHasMSParent(true);
				break;
			}
		}
	}


}
